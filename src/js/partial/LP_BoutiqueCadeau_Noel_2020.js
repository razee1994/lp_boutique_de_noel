//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
// Activer AOS pour les animations
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

if ($(window).width() > 1023) {
    AOS.init();
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
// Scroll menu
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
if($(".boutique-cadeau-2020-menu")){
    let menu = $(".boutique-cadeau-2020-menu");
    let menuH = $(".boutique-cadeau-2020-menu").height();
    
    let headerH ;
    let bodyH = $(".boutique-cadeau-2020").height();
    let footerH = $('#footer').height();
    let windowH = $(window).height();
    
    if ($(window).width() > 1023) {
    
        $(window).scroll(function () {
            let scrollH = $(window).scrollTop();
    
            if(scrollH > 99){
                headerH = 57;
            }else{
                headerH = $('#header').height()+40;
            }
    
            if (scrollH > ((bodyH+40 + headerH + (windowH - (headerH + menuH))) - windowH)) {
                menu.css("position", "absolute");
                menu.css("top", bodyH - menuH);
            } else {
                menu.css("position", "fixed");
                menu.css("top", headerH);
            }
    
        });
    
    }
    
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
// Scroll Menu anchor
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
if ($(window).width() > 1023 && $(".btn-buy-boutiqueC")) {

    $(".btn-buy-boutiqueC").first().addClass('active-archor');
    $(document).on("scroll", onScroll);

    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        $('#list-anchor a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('#list-anchor ul li a').removeClass("active-archor");
                currLink.addClass("active-archor");
            }
            else {
                currLink.removeClass("active-archor");
            }
        });
    }

}